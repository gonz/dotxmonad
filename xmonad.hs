import           XMonad
import           XMonad.Actions.NoBorders
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.SetWMName
import           XMonad.Layout.NoBorders
import           XMonad.Util.EZConfig
import           XMonad.Layout.LayoutModifier (ModifiedLayout)

myTerminal :: String
myTerminal = "urxvt"

rateSpawn :: FilePath
rateSpawn = "/home/gonz/bin/setrate.sh"

svorakA5Spawn :: FilePath
svorakA5Spawn = "/home/gonz/bin/svoraka5.sh"

seqwertySpawn :: FilePath
seqwertySpawn = "/home/gonz/bin/seqwerty.sh"

bgphoneticSpawn :: FilePath
bgphoneticSpawn = "/home/gonz/bin/bgphonetic.sh"

redshiftSpawn :: FilePath
redshiftSpawn = "redshift"

lowerVolumeSpawn = "pulseaudio-ctl down 1%"
muteSpawn = "pulseaudio-ctl mute"
raiseVolumeSpawn = "pulseaudio-ctl up 1%"

type MyLayout = Choose Tall (Choose (Mirror Tall) Full)

myLayout :: MyLayout a
myLayout = tiled ||| Mirror tiled ||| Full
  where tiled = Tall nmaster delta ratio
        nmaster = 1
        delta = 1/40
        ratio = 1/2

myStartupHook :: X ()
myStartupHook = do
  setWMName "LG3D"
  spawn rateSpawn
  spawn svorakA5Spawn
  spawn redshiftSpawn

myConfig :: XConfig (ModifiedLayout AvoidStruts
                     (ModifiedLayout SmartBorder MyLayout))
myConfig = def
   { layoutHook = avoidStruts $ smartBorders myLayout,
     borderWidth = 2,
     normalBorderColor  = "#000000", -- black
     focusedBorderColor = "#ff3f3f",
     terminal = myTerminal,
     startupHook = myStartupHook
   } `additionalKeysP`
   [("M-S-<F1>", spawn svorakA5Spawn),
     ("M-S-<F3>", spawn seqwertySpawn),
     ("M-S-<F2>", spawn bgphoneticSpawn),
     ("M-S-<F12>", withFocused toggleBorder),
     ("<XF86AudioLowerVolume>", spawn lowerVolumeSpawn),
     ("<XF86AudioMute>", spawn muteSpawn),
     ("<XF86AudioRaiseVolume>", spawn raiseVolumeSpawn)
     ]

main :: IO ()
main = xmonad myConfig
